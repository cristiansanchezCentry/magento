# Docker image for Magento 2



Este repo es una simplificacion de [este](https://hub.docker.com/r/alexcheng/magento2/tags/).
  Para su uso es necesario la instalacion de [Docker Compose](https://docs.docker.com/compose/).


## Pre-Installation

Previo a la instalacion, ingresar al .env file y selecciona aquella configuracion deseada, ya sea timezone, tipo de cambio o contraseñas.
## Installation
En la carpeta root ejecutar:
~~~
$ docker-compose up -d
~~~
El que se encargara de descargar y montar las imagenes de docker, y de ejecutar el Dockerfile.

Para instalar magento con los valores por defecto, ejecutar:
~~~
$ docker exec -it <container name> install-magento
~~~
Por defecto:
~~~
$ docker exec -it magento_web_1 install-magento
~~~
## Running
 Agregar la MAGENTO_URL que configuro en el .env file, a el archivo /etc/hosts usando el siguiente formato.
```
127.0.0.1    MAGENTO_URL
```
* Note que no debe agregar http:// o https:// previo a la ruta.

* Para entrar a la pestaña de configuracion de marketplace, ingresar a MAGENTO_URL/admin.

## Re-install
Para ver los contenedores detenidos y en proceso (para obtener nombres):
```
docker ps
```
Para eliminar el contenedor:
```
docker rm -f <container_name>
```
Para eliminar el volumen
```
docker volume rm <container_name>

```
